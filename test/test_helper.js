const mongoose = require('mongoose');
const JWT = require('jsonwebtoken')

module.exports.signToken = (id) => {
    return JWT.sign({
        iss: 'Siza-API',
        sub: id,
        iat: new Date().getTime(), 
        exp: new Date().setDate(new Date().getDate() + 1)
    }, 'jfekfgoewihrugfbruierughqierub' );
};

before(done => {
    mongoose.connect('mongodb+srv://myappdev:duwcas-vitsAf-hesfo1@cluster0-rkczc.gcp.mongodb.net/test?retryWrites=true&w=majority')
    mongoose.connection
        .once('open', () => done())
        .on('error', err => {
            console.warn('warning', err)
        })
});

beforeEach(done => {
    const { categories, activities, codes, feedback, suggestions, users, verifyTokens } = mongoose.connection.collections;
    Promise.all([ categories.drop(), activities.drop(), users.drop(), suggestions.drop()])
        .then(() => {
            done();
        })
        .catch(() => done());
})