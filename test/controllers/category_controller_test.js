const assert = require('assert');
const request = require('supertest');
const mongoose = require('mongoose');
const app = require('../../app');
const helper = require('../test_helper')
const User = require('../../models/user')

const Category = require('../../models/category')

describe( 'Category Controller' , () => {

    var token

    beforeEach((done) => {
        User.create({ email: 'test@test.nl', password: 'password', isVerified: true, isAdmin: true }).then(
            (user) => {
                token = helper.signToken(user._id)
                done()
            }
        )
        .catch(done)
    })



    describe('GET /', () => {        
        it('should get all categories, which should be one', (done) => {
            const newCategory = new Category({ name : "Name"})
            newCategory.save().then(
                () => {
                    request(app)
                        .get('/v1/categories')
                        .end((err, response) => {
                            assert(response.status === 200)
                            assert(response.body.categories.length === 1)
                            done()
                        })
                }
            )
        })
    })

    describe('GET /subs/:parentId', () => {
        it('should get all subcategories, which should be one', (done) => {
            const newCategory = new Category({ name : "Name"})
            const newSubCategory = new Category({ name : "Sub", parent: newCategory._id})
            Promise.all([ newCategory.save(), newSubCategory.save() ] ).then(
                () => {
                    request(app)
                        .get(`/v1/categories/subs/${newCategory._id}`)
                        .end((err, response) => {
                            assert(response.status === 200)
                            assert(response.body.subcategories.length === 1)
                            done()
                        })
                }
            )
        })
    })

    describe('POST /', () => {
        it('should create a new Category', (done) => {
            const newCategory = new Category({ name: 'new', image: 'imageURL'})

            request(app)
                .post(`/v1/admin/categories`)
                .set('Authorization', `${token}`)
                .send(newCategory)
                .end((err, response) => {
                    assert(response.status === 200)
                    const category = response.body.category
                    assert(category.name === newCategory.name)
                    assert(category.image === newCategory.image)
                    done()
                })
        })

        it('should return error when required parameter is missing', (done) => {
            const newCategory = new Category({image: 'imageURL'})

            request(app)
                .post(`/v1/admin/categories`)
                .set('Authorization', `${token}`)
                .send(newCategory)
                .end((err, response) => {
                    assert(response.status === 422)
                    done()
                })
        })

        it('should return error when parameter is invalid', (done) => {
            const newCategory = new Category({title: 'invalidImage', image: 123})

            request(app)
                .post(`/v1/admin/categories`)
                .set('Authorization', `${token}`)
                .send(newCategory)
                .end((err, response) => {
                    assert(response.status === 422)
                    done()
                })
        })
    })

    describe('PUT /:id', () => {
        it('should update existing category', (done) => {
            const newCategory = new Category({ name : "name", image: 'old'})
            newCategory.save().then(
                () => {
                    request(app)
                        .put(`/v1/admin/categories/${newCategory._id}`)
                        .set('Authorization', `${token}`)
                        .send({ 'image' : 'new' })
                        .end((err, response) => {
                            assert(response.status === 200)
                            assert(response.body.category.image === 'new')
                            assert(response.body.category.image !== newCategory.image)
                            Category.findById({ _id: newCategory._id}).then((category) => {
                                assert(category.image === 'new')
                                assert(category.image !== newCategory.image)
                                done();
                            })
                        })
                }
            )
        })
    })

    describe('DELETE /:id', () => {
        it('should delete existing category', (done) => {
            const newCategory = new Category({ name : "name", image: 'old'})
            newCategory.save().then(
                () => {
                    request(app)
                        .delete(`/v1/admin/categories/${newCategory._id}`)
                        .set('Authorization', `${token}`)
                        .end((err, response) => {
                            assert(response.status === 200)
                            Category.findById({ _id: newCategory._id}).then((category) => {
                                assert(category === null)
                                done();
                            })
                        })
                }
            )
        })
    })
})