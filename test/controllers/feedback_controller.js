const assert = require('assert');
const request = require('supertest');
const mongoose = require('mongoose');
const app = require('../../app');
const User = require('../../models/user')
const Feedback = require('../../models/feedback')
const Activity = require('../../models/activity')
const helper = require('../test_helper')


const newActivity = new Activity()
const newUser = new User()

const newFeedback = {message: 'message', userId: newUser._id, activityId: newActivity._id}

describe('Feedback Controller', () => {

    var adminToken

    beforeEach((done) => {
        User.create({ email: 'test@test.nl', password: 'password', isVerified: true, isAdmin: true }).then(
            (user) => {
                admin = user
                adminToken = helper.signToken(user._id)
                done()
            }
        )
        .catch(done)
    })

    describe('GET: v1/feedback/:id', () => {
        it('should get feedback of activity', (done) => {
            Feedback.create(newFeedback).then(
                (feedback) => {
                    request(app)
                        .get(`/v1/feedback/${newActivity._id}`)
                        .end((err, response) => {
                            assert(response.status === 200)
                            assert(response.body.feedback.length === 1)
                            assert(response.body.feedback[0]._id.toString() === feedback._id.toString())
                            done()
                        })
                    
                }
            )
        })
    })

    describe('POST: v1/feedback/:id', () => {
        it('should post feedback to activity', (done) => {
            request(app)
            .post(`/v1/feedback/${newActivity._id}`)
            .set('Authorization', `${adminToken}`)
            .send(newFeedback)
            .end((err, response) => {
                assert(response.status === 200)
                assert(response.body.feedback.message === newFeedback.message)
                done()
            })
        })

        it('shouldnt post feedback to activity', (done) => {
            request(app)
            .post(`/v1/feedback/${newActivity._id}`)
            .set('Authorization', `${adminToken}`)
            .send({})
            .end((err, response) => {
                assert(response.status === 422)
                assert(response.body.msg === 'feedback validation failed: message: Path `message` is required.')
                done()
            })
        })

        it('shouldnt post feedback when not authorized', (done) => {
            request(app)
            .post(`/v1/feedback/${newActivity._id}`)
            .send(newFeedback)
            .end((err, response) => {
                assert(response.status === 422)
                done()
            })
        })
    })

    describe('PUT: v1/feedback/:id', () => {
        var token = helper.signToken(new User()._id)
        var createdToken = helper.signToken(newUser._id)
        var feedback

        beforeEach((done) => {
            Feedback.create(newFeedback).then(
                (createdFeedback) => {
                    feedback = createdFeedback
                    done()
                }
            )
        })

        it('should put feedback with :id when user is user who created feedback', (done) => {
            var updatedMessage = 'updated'
            request(app)
            .put(`/v1/feedback/${feedback._id}`)
            .set('Authorization', `${createdToken}`)
            .send({message: updatedMessage})
            .end((err, response) => {
                assert(response.status === 200)
                Feedback.findById(feedback._id).then(
                    (found) => {
                        assert(found != null)
                        assert(found.userId.toString() == newUser._id.toString())
                        assert(found.message == updatedMessage)
                        done()
                    }
                )
                
                
            })
        })

        
        it('shouldnt put feedback with :id when user isnt user who created feedback', (done) => {
            var updatedMessage = 'updated'
            request(app)
            .put(`/v1/feedback/${feedback._id}`)
            .set('Authorization', `${token}`)
            .send({message: updatedMessage})
            .end((err, response) => {
                assert(response.status !== 200)
                Feedback.findById(feedback._id).then(
                    (found) => {
                        assert(found != null)
                        assert(found.userId.toString() == newUser._id.toString())
                        assert(found.message !== updatedMessage)
                        assert(found.message === newFeedback.message)
                        done()
                    }
                )
                
                
            })
        })
       
    })

    describe('DELETE: v1/feedback/:id', () => {
        var token = helper.signToken(new User()._id)
        var createdToken = helper.signToken(newUser._id)
        var feedback

        beforeEach((done) => {
            Feedback.create(newFeedback).then(
                (createdFeedback) => {
                    feedback = createdFeedback
                    done()
                }
            )
        })

        it('should delete feedback with :id when user is user who created feedback', (done) => {
            request(app)
            .delete(`/v1/feedback/${feedback._id}`)
            .set('Authorization', `${createdToken}`)
            .end((err, response) => {
                assert(response.status === 200)
                Feedback.findById(feedback._id).then(
                    (found) => {
                        done()
                    }
                )
            })
        })

        it('should delete feedback with :id when user is admin', (done) => {
            request(app)
            .delete(`/v1/feedback/${feedback._id}`)
            .set('Authorization', `${adminToken}`)
            .end((err, response) => {
                assert(response.status === 200)
                Feedback.findById(feedback._id).then(
                    (found) => {
                        done()
                    }
                )
            })
        })

        
        it('shouldnt delete feedback with :id when user isnt user who created feedback', (done) => {
            request(app)
            .delete(`/v1/feedback/${feedback._id}`)
            .set('Authorization', `${token}`)
            .end((err, response) => {
                assert(response.status !== 200)
                Feedback.findById(feedback._id).then(
                    (found) => {
                        assert(found !== null)
                        done()
                    }
                )
            })
        })
       
    })

})

// router.put('/:id', FeedbackController.updateFeedback )
// router.delete('/:id', FeedbackController.deleteFeedback )