const assert = require('assert');
const request = require('supertest');
const mongoose = require('mongoose');
const app = require('../../app');
const User = require('../../models/user')
const VerifyToken = require('../../models/verifyToken')

email = 'newtest@test.com'
password = 'password'

describe('AuthController', () => {
    describe('POST: /v1/auth/signup', () => {
        it('should create new user who must first validate email and who isnt admin', (done) => {
            request(app)
            .post('/v1/auth/signup')
            .send({'email' : 'newemail@email.com', 'password' : password})
            .end((err, response) => {
                assert(response.status === 200)
                User.findOne({ email: 'newemail@email.com'}).then(
                    (foundUser) => {
                        assert(foundUser != null)
                        assert(!foundUser.isVerified)
                        assert(!foundUser.isAdmin)
                        done();
                    }
                )
            })
        })

        it('shouldnt create user', (done) => {
            User.create({'email' : email, 'password': password}).then(
                (user) => {
                    assert(!user.isVerified)
                    request(app)
                    .post('/v1/auth/signup')
                    .send({'email' : email, 'password' : password})
                    .end((err, response) => {
                        assert(response.status === 400)
                        done()
                    })
                }
            )
        })
    })

    describe('POST: /v1/auth/confirmation/:token', () => {
        it('should confirm token', (done) => {
            User.create({'email' : email, 'password': password}).then(
                (user) => {
                    assert(!user.isVerified)
                    VerifyToken.create({_userId: user._id }).then(
                        (token) => {
                            
                            request(app)
                            .post(`/v1/auth/confirmation/${token._id}`)
                            .end((err, result) => {
                                
                                assert(result.status == 200)
                                assert(result.body.msg == 'The account has been verified. Please log in.')
                                User.findOne({ email: email}).then(
                                    (foundUser) => {
                                        assert(foundUser != null)
                                        assert(foundUser.isVerified)
                                        assert(!foundUser.isAdmin)
                                        done();
                                    }
                                )
                            })
                        }
                    )
                }
            )
        })
    })

    describe('POST: /v1/auth/login', () => {
        it('should login user', (done) => {
            User.create({'email' : email, 'password': password, isVerified: true}).then(
                (user) => {
                    request(app)
                    .post('/v1/auth/login')
                    .send({'email' : email, 'password': password})
                    .end((err, response) => {
                        assert(response.status === 200)
                        assert(response.body.accesToken != null)
                        assert(response.body.refreshToken != null)
                        assert(response.body.user != null)
                        done()
                    })
            })
            
        })

        it('shouldnt login user when user doesnt exists', (done) => {
            
            request(app)
            .post('/v1/auth/login')
            .send({'email' : email, 'password': password})
            .end((err, response) => {
                assert(response.status === 401)
                done()
            })
        
        })
    })

    describe('POST: /v1/auth/refreshtoken', () => {
        it('shoud refresh accesToken when user is signed in', (done) => {
            User.create({'email' : email, 'password': password, isVerified: true}).then(
                (user) => {
                    request(app)
                    .post('/v1/auth/login')
                    .send({'email' : email, 'password': password})
                    .end((err, response) => {
                        assert(response.status === 200)
                        assert(response.body.accesToken != null)
                        assert(response.body.refreshToken != null)
                        assert(response.body.user != null)
                        request(app)
                        .post('/v1/auth/refreshtoken')
                        .send({'email': email, 'refreshToken': response.body.refreshToken})
                        .end((err, response) => {
                            assert(response.status === 200)
                            assert(response.body.accesToken != null)
                            done()
                        })
                    })
            })
        })

        it('shoudnt refresh accesToken when user isnt signed in and refresh token is not valid', (done) => {
            User.create({'email' : email, 'password': password, isVerified: true}).then(
                (user) => {
                    request(app)
                    .post('/v1/auth/refreshtoken')
                    .send({'email': email, 'refreshToken': 'randomaccestokenwitchshouldntwork'})
                    .end((err, response) => {
                        assert(response.status === 401)
                        assert(response.body.accesToken == null)
                        done()
                    })
            })
        })
    })

})