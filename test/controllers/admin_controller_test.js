const assert = require('assert');
const request = require('supertest');
const mongoose = require('mongoose');
const app = require('../../app');
const User = require('../../models/user')
const Suggestion = require('../../models/suggestion')
const Activity = require('../../models/activity')
const Feedback = require('../../models/feedback')
const Category = require('../../models/category')
const helper = require('../test_helper')

const newUser = new User()

describe('Admin Controller', () => {

    var token
    var adminToken

    beforeEach((done) => {
        User.create({ email: 'test@test.nl', password: 'password', isVerified: true, isAdmin: true }).then(
            (user) => {
                adminToken = helper.signToken(user._id)
                User.create({ email: 'test@test.nl', password: 'password', isVerified: true, isAdmin: false }).then(
                    (user) => {
                        token = helper.signToken(user._id)
                        done()
                    }
                )
                .catch(done)
            }
        )
        .catch(done)
    })

    describe('activities', () => {
        var catgoryOfActivity = new Category()
        var activityBody = {
            title: "title",
            goal: "goal",
            image: "https://o.aolcdn.com/images/dims?quality=85&image_uri=https%3A%2F%2Fo.aolcdn.com%2Fimages%2Fdims%3Fcrop%3D904%252C507%252C0%252C0%26quality%3D85%26format%3Djpg%26resize%3D1600%252C897%26image_uri%3Dhttps%253A%252F%252Fs.yimg.com%252Fos%252Fcreatr-uploaded-images%252F2019-09%252Fa252c700-d091-11e9-98d7-456d0782809a%26client%3Da1acac3e1b3290917d92%26signature%3Dbfd0e993df5297c08f967061614c8160d0d9ccd8&client=amp-blogside-v2&signature=5cf955b9a3304615eae2ed4df51146dca86f2421",
            material: "material",
            activity: "activity",
            tooHard: "tooHard",
            tooEasy: "tooEasy",
            setUp: "setUp",
            setUpImage: "https://o.aolcdn.com/images/dims?quality=85&image_uri=https%3A%2F%2Fo.aolcdn.com%2Fimages%2Fdims%3Fcrop%3D904%252C507%252C0%252C0%26quality%3D85%26format%3Djpg%26resize%3D1600%252C897%26image_uri%3Dhttps%253A%252F%252Fs.yimg.com%252Fos%252Fcreatr-uploaded-images%252F2019-09%252Fa252c700-d091-11e9-98d7-456d0782809a%26client%3Da1acac3e1b3290917d92%26signature%3Dbfd0e993df5297c08f967061614c8160d0d9ccd8&client=amp-blogside-v2&signature=5cf955b9a3304615eae2ed4df51146dca86f2421",
            duration: 60,
            amountOfPeople: 10,
            wheelchair: 'true',
            pointsForAttention: "pointsForAttention",
            category: catgoryOfActivity._id
        }

        describe('POST /v1/admin/activities', () => {
            it('should post a new activity', (done) => {
                request(app)
                .post('/v1/admin/activities')
                .set('Authorization', `${adminToken}`)
                .send(activityBody)
                .end((err, response) => {
                    assert(response.status === 200)
                    const activity = response.body.activity
                    assert(activity.goal === activityBody.goal)
                    assert(activity.material === activityBody.material)
                    assert(activity.activity === activityBody.activity)
                    assert(activity.setup === activityBody.setup)
                    assert(activity.pointsForAttention === activityBody.pointsForAttention)
                    assert(activity.image === activityBody.image)
                    Activity.findById(activity._id).then(
                        (found) => {
                            assert(found != null)
                            done()
                        }
                    )
                    
                })
            })

            it('shouldnt post a new activity when data is invalid', (done) => {
                request(app)
                .post('/v1/admin/activities')
                .set('Authorization', `${adminToken}`)
                .send({})
                .end((err, response) => {
                    assert(response.status === 422)
                    Activity.findOne().then(
                        (found) => {
                            assert(found == null)
                            done()
                        }
                    )
                   
                })
            })

            it('should not post a new activity when user is not admin', (done) => {
                request(app)
                .post('/v1/admin/activities')
                .set('Authorization', `${token}`)
                .send(activityBody)
                .end((err, response) => {
                    assert(response.status === 401)
                    Activity.findOne().then(
                        (found) => {
                            assert(found == null)
                            done()
                        }
                    )
                })
            })
        })

        describe('PUT /v1/admin/activities/:id', () => {
            var activity
            beforeEach((done) => {
                Activity.create(activityBody).then(
                    (created) => {
                        activity = created
                        done()
                    }
                )
                .catch(done)
            })

            it('should update existing activity', (done) => {
                var updatedValue = 'updatedActivity'
                request(app)
                .put(`/v1/admin/activities/${activity._id}`)
                .set('Authorization', `${adminToken}`)
                .send({activity: updatedValue})
                .end((err, response) => {
                    assert(response.status == 200)
                    assert(response.body.activity.activity == updatedValue)
                    Activity.findById(response.body.activity._id).then(
                        (found) => {
                            assert(found != null)
                            assert(found.activity === updatedValue)
                            done()
                        }
                    )
                    
                })
            })

            it('shouldnt update existing activity when user is not admin', (done) => {
                var updatedValue = 'updatedActivity'
                request(app)
                .put(`/v1/admin/activities/${activity._id}`)
                .set('Authorization', `${token}`)
                .send({activity: updatedValue})
                .end((err, response) => {
                    assert(response.status !== 200)
                    Activity.findById(activity._id).then(
                        (found) => {
                            assert(found != null)
                            assert(!found.activity !== updatedValue)
                            done()
                        }
                    )
                    
                })
            })
        })

        describe('DELETE /v1/admin/activities/:id', () => {
            var activity
            beforeEach((done) => {
                Activity.create(activityBody).then(
                    (created) => {
                        activity = created
                        done()
                    }
                )
                .catch(done)
            })

            it('should delete existing activity', (done) => {
                request(app)
                .delete(`/v1/admin/activities/${activity._id}`)
                .set('Authorization', `${adminToken}`)
                .end((err, response) => {
                    assert(response.status === 200)
                    Activity.findById(activity._id).then(
                        (found) => {
                            assert(found === null)
                            done()
                        }
                    )
                    
                })
            })

            it('shouldnt delete existing activity when user is not admin', (done) => {
                request(app)
                .delete(`/v1/admin/activities/${activity._id}`)
                .set('Authorization', `${token}`)
                .end((err, response) => {
                    assert(response.status !== 200)
                    Activity.findById(activity._id).then(
                        (found) => {
                            assert(found !== null)
                            done()
                        }
                    )
                    
                })
            })
        })
    })

    describe('categories', () => {
        var category 
        var categoryBody = { name: 'new', image: 'imageURL' }

        beforeEach((done) => {
            Category.create(categoryBody).then(
                (newCategory) => {
                    category = newCategory
                    done()
                }
            )
        })

        describe('POST /v1/admin/categories', () => {
            it('should create a new category', (done) => {
                request(app)
                .post('/v1/admin/categories')
                .set('Authorization', `${adminToken}`)
                .send(categoryBody)
                .end((err, response) => {
                    assert(response.status === 200)
                    const category = response.body.category
                    assert(category.name === categoryBody.name)
                    Category.findById(category._id).then(
                        (found) => {
                            assert(found != null)
                            done()
                        }
                    )
                    
                })
            })

            it('shouldnt create a new category when user is not admin', (done) => {
                request(app)
                .post('/v1/admin/categories')
                .set('Authorization', `${token}`)
                .send(categoryBody)
                .end((err, response) => {
                    assert(response.status !== 200)
                    Category.find().then(
                        (found) => {
                            assert(found.length === 1)
                            done()
                        }
                    )
                    
                })
            })

            it('shouldnt create a new category when data is invalid', (done) => {
                request(app)
                .post('/v1/admin/categories')
                .set('Authorization', `${token}`)
                .send({name : null})
                .end((err, response) => {
                    assert(response.status !== 200)
                    Category.find().then(
                        (found) => {
                            assert(found.length === 1)
                            done()
                        }
                    )
                    
                })
            })
        })

        describe('PUT /v1/admin/categories/:id', () => {
            it('should update existing category', (done) => {
                var updatedValue = 'updatedCategory'
                request(app)
                .put(`/v1/admin/categories/${category._id}`)
                .set('Authorization', `${adminToken}`)
                .send({name: updatedValue})
                .end((err, response) => {
                    assert(response.status == 200)
                    assert(response.body.category.name == updatedValue)
                    Category.findById(response.body.category._id).then(
                        (found) => {
                            assert(found != null)
                            assert(found.name === updatedValue)
                            done()
                        }
                    )
                    
                })
            })

            it('shouldnt update existing category when user isnt admin', (done) => {
                var updatedValue = 'updatedCategory'
                request(app)
                .put(`/v1/admin/categories/${category._id}`)
                .set('Authorization', `${token}`)
                .send({name: updatedValue})
                .end((err, response) => {
                    assert(response.status !== 200)
                    Category.findById(category._id).then(
                        (found) => {
                            assert(found != null)
                            assert(found.name === category.name)
                            done()
                        }
                    )
                    
                })
            })
        })

        describe('DELETE /v1/admin/categories/:id', () => {
            it('should delete existing category', (done) => {
                request(app)
                .delete(`/v1/admin/categories/${category._id}`)
                .set('Authorization', `${adminToken}`)
                .end((err, response) => {
                    assert(response.status == 200)
                    Category.findById(category._id).then(
                        (found) => {
                            assert(found === null)
                            done()
                        }
                    )
                    
                })
            })

            it('should delete existing category', (done) => {
                request(app)
                .delete(`/v1/admin/categories/${category._id}`)
                .set('Authorization', `${token}`)
                .end((err, response) => {
                    assert(response.status !== 200)
                    Category.findById(category._id).then(
                        (found) => {
                            assert(found !== null)
                            done()
                        }
                    )
                    
                })
            })
        })
    })

    describe('users', () => {
        var user

        beforeEach((done) => {
            User.create({'email' : 'newemail@email.com', 'password' : 'password'}).then(
                (created) => {
                    user = created
                    done()
                }
            )
            .catch(done)
        })

        describe('GET /v1/admin/users', () => {
            it('should get all users', (done) => {
                request(app)
                .get(`/v1/admin/users`)
                .set('Authorization', `${adminToken}`)
                .end((err, response) => {
                    assert(response.status == 200)
                    assert(response.body.users.length === 3)
                    done()
                })
            })

            it('shouldnt get all users when user is not admin', (done) => {
                request(app)
                .get(`/v1/admin/users`)
                .set('Authorization', `${token}`)
                .end((err, response) => {
                    assert(response.status !== 200)
                    assert(!response.body.users)
                    done()
                })
            })
        })

        describe('GET /v1/admin/users/:id', () => {
            it('should get user by id', (done) => {
                request(app)
                .get(`/v1/admin/users/${user._id}`)
                .set('Authorization', `${adminToken}`)
                .end((err, response) => {
                    assert(response.status == 200)
                    assert(response.body.user !== null)
                    assert(response.body.user._id.toString() === user._id.toString())
                    assert(response.body.user.email === user.email)
                    done()
                })
            })

            it('should get user by id', (done) => {
                request(app)
                .get(`/v1/admin/users/${user._id}`)
                .set('Authorization', `${token}`)
                .end((err, response) => {
                    assert(response.status !== 200)
                    done()
                })
            })
        })

        describe('GET /v1/admin/admins', () => {
            it('should get all users', (done) => {
                request(app)
                .get(`/v1/admin/admins`)
                .set('Authorization', `${adminToken}`)
                .end((err, response) => {
                    assert(response.status == 200)
                    assert(response.body.admins.length === 1)
                    assert(response.body.admins[0].isAdmin)
                    done()
                })
            })

            it('should get all users', (done) => {
                request(app)
                .get(`/v1/admin/admins`)
                .set('Authorization', `${token}`)
                .end((err, response) => {
                    assert(response.status !== 200)
                    assert(response.error.text === 'access denied')
                    done()
                })
            })
        })

        describe('POST /v1/admin/:id/:isAdmin', () => {
            it('should set user to admin', (done) => {
                request(app)
                .post(`/v1/admin/${user._id}/true`)
                .set('Authorization', `${adminToken}`)
                .end((err, response) => {
                    assert(response.status == 200)
                    assert(response.body.user.isAdmin)
                    assert(response.body.user._id,toString() === user._id.toString())
                    User.findById(user._id).then(
                        (found) => {
                            assert(found !== null)
                            assert(found.isAdmin)
                            done()
                        }
                    )
                })
            })

            it('should return error when last admin would be set to user', (done) => {
                request(app)
                .post(`/v1/admin/${user._id}/false`)
                .set('Authorization', `${adminToken}`)
                .end((err, response) => {
                    assert(response.status !== 200)
                    assert(response.error.text === '{"msg":"there must be at least one admin"}')
                    done()
                })
            })

            it('shouldnt set user to admin when user isnt admin', (done) => {
                request(app)
                .post(`/v1/admin/${user._id}/true`)
                .set('Authorization', `${token}`)
                .end((err, response) => {
                    assert(response.status !== 200)
                    User.findById(user._id).then(
                        (found) => {
                            assert(found !== null)
                            assert(!found.isAdmin)
                            done()
                        }
                    )
                })
            })
        })
    })

    describe('suggestions', () => {

        var suggestion
        const newSuggestion = {message: 'message', userId: newUser._id}

        beforeEach((done) => {
            Suggestion.create(newSuggestion).then(
                (created) => {
                    suggestion = created
                    done()
                }
            )
        })

        describe('GET /v1/admin/suggestions', () => {

            it('should get all suggestions', (done) => {
                request(app)
                .get(`/v1/admin/suggestions`)
                .set('Authorization', `${adminToken}`)
                .end((err, response) => {
                    assert(response.status == 200)
                    assert(response.body.suggestions.length === 1)
                    done()
                })
            })

            it('should get all suggestions', (done) => {
                request(app)
                .get(`/v1/admin/suggestions`)
                .set('Authorization', `${token}`)
                .end((err, response) => {
                    assert(response.status !== 200)
                    done()
                })
            })

        })

        describe('PUT /v1/admin/suggestions/:id/read/:read', () => {
            it('should set suggestion to read', (done) => {
                request(app)
                .put(`/v1/admin/suggestions/${suggestion._id}/read/true`)
                .set('Authorization', `${adminToken}`)
                .end((err, response) => {
                    assert(response.status == 200)
                    assert(response.body.suggestion._id,toString() === suggestion._id.toString())
                    Suggestion.findById(suggestion._id).then(
                        (found) => {
                            assert(found !== null)
                            assert(found.read)
                            done()
                        }
                    )
                })
            })

            it('should set suggestion to unread', (done) => {
                request(app)
                .put(`/v1/admin/suggestions/${suggestion._id}/read/false`)
                .set('Authorization', `${adminToken}`)
                .end((err, response) => {
                    assert(response.status == 200)
                    assert(response.body.suggestion._id,toString() === suggestion._id.toString())
                    Suggestion.findById(suggestion._id).then(
                        (found) => {
                            assert(found !== null)
                            assert(!found.read)
                            done()
                        }
                    )
                })
            })

            it('shouldnt set suggestion to read when user isnt admin', (done) => {
                request(app)
                .put(`/v1/admin/suggestions/${suggestion._id}/read/true`)
                .set('Authorization', `${token}`)
                .end((err, response) => {
                    assert(response.status !== 200)
                    Suggestion.findById(suggestion._id).then(
                        (found) => {
                            assert(found !== null)
                            assert(found.read == suggestion.read)
                            done()
                        }
                    )
                })
            })
        })
    })

})