const assert = require('assert');
const helper = require('../test_helper')
const request = require('supertest');
const mongoose = require('mongoose');
const app = require('../../app');
const authController = require('../../controllers/auth_controller')
const User = require('../../models/user')
const Activity = require('../../models/activity')
const Category = require('../../models/category')

describe( 'Activity Controller' , () => {

    var newActivity
    var newActivity2
    var catgoryOfActivity
    var token

    beforeEach((done) => {
        catgoryOfActivity = new Category({ name: "category", image: "image"})
        activityBody = {
            title: "title",
            goal: "goal",
            image: "https://o.aolcdn.com/images/dims?quality=85&image_uri=https%3A%2F%2Fo.aolcdn.com%2Fimages%2Fdims%3Fcrop%3D904%252C507%252C0%252C0%26quality%3D85%26format%3Djpg%26resize%3D1600%252C897%26image_uri%3Dhttps%253A%252F%252Fs.yimg.com%252Fos%252Fcreatr-uploaded-images%252F2019-09%252Fa252c700-d091-11e9-98d7-456d0782809a%26client%3Da1acac3e1b3290917d92%26signature%3Dbfd0e993df5297c08f967061614c8160d0d9ccd8&client=amp-blogside-v2&signature=5cf955b9a3304615eae2ed4df51146dca86f2421",
            material: "material",
            activity: "activity",
            tooHard: "tooHard",
            tooEasy: "tooEasy",
            setUp: "setUp",
            setUpImage: "https://o.aolcdn.com/images/dims?quality=85&image_uri=https%3A%2F%2Fo.aolcdn.com%2Fimages%2Fdims%3Fcrop%3D904%252C507%252C0%252C0%26quality%3D85%26format%3Djpg%26resize%3D1600%252C897%26image_uri%3Dhttps%253A%252F%252Fs.yimg.com%252Fos%252Fcreatr-uploaded-images%252F2019-09%252Fa252c700-d091-11e9-98d7-456d0782809a%26client%3Da1acac3e1b3290917d92%26signature%3Dbfd0e993df5297c08f967061614c8160d0d9ccd8&client=amp-blogside-v2&signature=5cf955b9a3304615eae2ed4df51146dca86f2421",
            duration: 60,
            amountOfPeople: 10,
            wheelchair: true,
            pointsForAttention: "pointsForAttention",
            category: catgoryOfActivity._id
        }
        
        newActivity = new Activity(activityBody)
        newActivity2 = new Activity(activityBody)

        User.create({ email: 'test@test.nl', password: 'password', isVerified: true, isAdmin: true }).then(
            (user) => {
                token = helper.signToken(user._id)
                done()
            }
        )
        .catch(done)
    })

    describe('GET /', () => {
        it('should get all activities, which should be two', (done) => {
            
            Promise.all([ catgoryOfActivity.save(), newActivity.save(), newActivity2.save() ]).then(
                () => {
                    request(app)
                        .get('/v1/activities')
                        .end((err, response) => {
                            assert(response.status === 200)
                            done() 
                        })
                }
            )
        })
    })

    describe('GET Specific/', () => {
        it('should get one activity, which should be one', (done) => {
            Promise.all([ catgoryOfActivity.save(), newActivity.save(), newActivity2.save() ]).then(
                () => {
                    request(app)
                        .get('/v1/activities/' + newActivity2._id)
                        .end((err, response) => {
                            assert(response.status === 200)
                            done()
                        })
                }
            )
        })
    })

    describe('POST /', () => {
        it('should create a new Activity', (done) => {
            request(app)
                .post(`/v1/admin/activities`)
                .set('Authorization', `${token}`)
                .send(newActivity)
                .end((err, response) => {
                    console.log(response.status)
                    console.log(response.body)
                    console.log(response.error)
                    assert(response.status === 200)
                    const responseActivity = response.body.activity
                    Activity.findById({ _id: responseActivity._id}).then(
                        (activity) => {
                            assert(activity._id.toString() === responseActivity._id.toString())
                            assert(activity.goal === newActivity.goal)
                            assert(activity.material === newActivity.material)
                            assert(activity.activity === newActivity.activity)
                            assert(activity.setup === newActivity.setup)
                            assert(activity.pointsForAttention === newActivity.pointsForAttention)
                            assert(activity.image === newActivity.image)
                            done()
                        }
                    )
                })
        })


        it('should return error when required parameter is missing', (done) => {
            newActivity.activity = null
            request(app)
                .post(`/v1/admin/activities`)
                .set('Authorization', `${token}`)
                .send(newActivity)
                .end((err, response) => {
                    assert(response.status === 422)
                    assert(response.error)
                    done()
                })
        })

        it('should return error when user is not authorized', (done) => {
            newActivity.activity = null
            request(app)
                .post(`/v1/admin/activities`)
                .send(newActivity)
                .end((err, response) => {
                    assert(response.status === 401)
                    assert(response.error)
                    done()
                })
        })

     })

    describe('PUT /:id', () => {
        it('should update existing activity', (done) => {
            newActivity.save().then(
                (activity) => {
                    request(app)
                        .put(`/v1/admin/activities/${activity._id}`)
                        .set('Authorization', `${token}`)
                        .send({ 'image' : 'new' })
                        .end((err, response) => {
                            assert(response.status === 200)
                            assert(response.body.activity.image === 'new')
                            assert(response.body.activity.image !== newActivity.image)
                            Activity.findById({ _id: newActivity._id}).then((activity) => {
                                assert(activity.image === 'new')
                                assert(activity.image !== newActivity.image)
                                done();
                            })
                        })
                }
            )
        })
    })

    describe('DELETE /:id', () => {
        it('should delete existing category', (done) => {
            newActivity.save().then(
                () => {
                    request(app)
                        .delete(`/v1/admin/activities/${newActivity._id}`)
                        .set('Authorization', `${token}`)
                        .end((err, response) => {
                            assert(response.status === 200)
                            Activity.findById({ _id: newActivity._id}).then((activity) => {
                                assert(activity === null)
                                done();
                            })
                        })
                }
            )
        })
    })
})