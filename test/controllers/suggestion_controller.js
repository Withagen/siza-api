const assert = require('assert');
const request = require('supertest');
const mongoose = require('mongoose');
const app = require('../../app');
const User = require('../../models/user')
const Suggestion = require('../../models/suggestion')
const Activity = require('../../models/activity')
const helper = require('../test_helper')

const newUser = new User()

const newSuggestion = {message: 'message', userId: newUser._id}

describe('Suggestion Controller', () => {

    var token
    var adminToken

    beforeEach((done) => {
        User.create({ email: 'test@test.nl', password: 'password', isVerified: true, isAdmin: true }).then(
            (user) => {
                adminToken = helper.signToken(user._id)
                User.create({ email: 'test@test.nl', password: 'password', isVerified: true, isAdmin: false }).then(
                    (user) => {
                        token = helper.signToken(user._id)
                        done()
                    }
                )
                .catch(done)
            }
        )
        .catch(done)
    })

    describe('GET: v1/suggestions/:id', () => {
        it('should get suggestion by id', (done) => {
            Suggestion.create(newSuggestion).then(
                (suggestion) => {
                    request(app)
                    .get(`/v1/suggestions/${suggestion._id}`)
                    .end((err, response) => {
                        assert(response.status === 200)
                        assert(response.body.suggestion._id.toString() === suggestion._id.toString())
                        done()
                    })
                    
                }
            )
        })
    })

    describe('POST: v1/suggestions', () => {
        it('should post suggestion', (done) => {
            request(app)
            .post(`/v1/suggestions`)
            .set('Authorization', `${token}`)
            .send(newSuggestion)
            .end((err, response) => {
                assert(response.status === 200)
                assert(response.body.suggestion.read === false)
                assert(response.body.suggestion.message === newSuggestion.message)
                assert(response.body.suggestion.userId)
                done()
            })
        })

        it('shouldnt post suggestion when not authorized', (done) => {
            request(app)
            .post(`/v1/suggestions`)
            .send(newSuggestion)
            .end((err, response) => {
                assert(response.status === 422)
                done()
            })
        })
    })
    
    // router.delete('/:id', SuggestionController.deleteSuggestion )
    

})