const express = require('express')
const router = express.Router()

const FeedbackController = require('../controllers/feedback_controller')

router.get('/:id', FeedbackController.getFeedbackOfActivity )
router.post('/:activityId/:parentId', FeedbackController.postFeedback )
router.post('/:activityId', FeedbackController.postFeedback )
router.put('/:id', FeedbackController.updateFeedback )
router.delete('/:id', FeedbackController.deleteFeedback )

module.exports = router