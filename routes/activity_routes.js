const express = require('express')
const router = express.Router()

const ActivityController = require('../controllers/activity_controller')


router.get('', ActivityController.getAll)
router.get('/:activityId', ActivityController.getSpecific)
router.get('/category/:categoryId', ActivityController.getAllSpecific)
// router.post('', ActivityController.post)
// router.put('/:activityId', ActivityController.put)
// router.delete('/:activityId', ActivityController.delete)


module.exports = router