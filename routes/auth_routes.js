const express = require('express')
const router = express.Router()
const passport = require('passport');
const passportConf = require('../password'); //DONT REMOVE

const AuthController = require('../controllers/auth_controller')
const UsersController = require('../controllers/users_controller')

router.post('/signup', AuthController.signup);
router.post('/confirmation/:token', AuthController.confirmationPost);

router.post('/adminregister', passport.authenticate('jwt', { session: false }), AuthController.registerNewAdmin) 
router.post('/login', passport.authenticate('local', { session: false }), AuthController.login) 

router.get('/jwttest', passport.authenticate('jwt', { session: false }), AuthController.jwt)
router.post('/refreshtoken', AuthController.refreshToken)

router.get('/me', passport.authenticate('jwt', { session: false }), UsersController.getMe)
router.get('/:id', passport.authenticate('jwt', { session: false }), UsersController.getById)

module.exports = router