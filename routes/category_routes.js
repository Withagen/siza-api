const express = require('express')
const router = express.Router()

const CategoryController = require('../controllers/category_controller')

router.get('', CategoryController.get )
router.get('/withoutsub/:id', CategoryController.getWithoutChildrenOfCategory )
router.get('/getParentName', CategoryController.getCategoriesParentName )
router.get('/noParent', CategoryController.getCategoriesWithoutParent )
router.get('/subs/:parentId', CategoryController.getSubOfParent )
router.get('/getParentName/:id', CategoryController.getSpecificCategoryParentName )
router.get('/:id', CategoryController.getSpecificCategory )

module.exports = router