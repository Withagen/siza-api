const express = require('express')
const router = express.Router()
const multer = require('multer')
const randtoken = require('rand-token')

const ActivityController = require('../controllers/activity_controller')
const CategoryController = require('../controllers/category_controller')
const UsersController = require('../controllers/users_controller')
const SuggestionController = require('../controllers/suggestion_controller')


const storage = multer.diskStorage(
    {
        destination: function(req, file, next) {
            console.log(file.mimetype)
            next(null, 'images/activity')
        },
        filename: function (req, file, next) {
            var token = randtoken.uid(16) 
            next(null, token + file.originalname)
        }
    }
)

const categoryStorage = multer.diskStorage(
    {
        destination: function(req, file, next) {
            console.log(file.mimetype)
            next(null, 'images/category')
        },
        filename: function (req, file, next) {
            var token = randtoken.uid(16) 
            next(null, token + file.originalname)
        }
    
    }
)

const upload = multer({storage: storage})


const uploadCatgory = multer({storage: categoryStorage})


//Activities
router.post('/activities', upload.array('images'), ActivityController.post)
router.put('/activities/:activityId', upload.array('images'), ActivityController.put)
router.delete('/activities/:activityId', ActivityController.delete)

//Categories
router.post('/categories', uploadCatgory.single('categoryImage'), CategoryController.post )
router.put('/categories/:categoryId', uploadCatgory.single('categoryImage'), CategoryController.put )
router.delete('/categories/:categoryId', CategoryController.delete )

//Users
router.get('/users',  UsersController.getAll)
router.get('/users/:id',  UsersController.getByIdAdmin)
router.get('/admins',  UsersController.getAdmins)
router.post('/:id/:isAdmin', UsersController.setAdmin)

//Suggestion
router.get('/suggestions', SuggestionController.getAll)
router.put('/suggestions/:id/read/:read', SuggestionController.updateRead )

module.exports = router