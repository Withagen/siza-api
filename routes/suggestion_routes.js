const express = require('express')
const router = express.Router()

const SuggestionController = require('../controllers/suggestion_controller')

router.get('/:id', SuggestionController.getSuggestionById )
router.post('', SuggestionController.createSuggestion )
router.delete('/:id', SuggestionController.deleteSuggestion )


module.exports = router