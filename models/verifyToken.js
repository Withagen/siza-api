const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs'); 

const VerifyTokenSchema = new Schema({
    _userId: { 
        type: mongoose.Schema.Types.ObjectId, 
        required: true, 
        ref: 'users' 
    },
    // token: { 
    //     type: String, 
    //     required: true },
    createdAt: { 
        type: Date, 
        required: true, 
        default: Date.now, 
        expires: 43200 
    }
})

const VerifyToken = mongoose.model('verifyTokens', VerifyTokenSchema)


module.exports = VerifyToken