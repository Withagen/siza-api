const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CodeSchema = new Schema({
    code: {
        unique: true,
        type: String,
        required: true
    },
    endDate: {
        type: Date,
        required: true
    }
})

const Code = mongoose.model('codes', CodeSchema)

module.exports = Code