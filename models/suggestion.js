const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SuggestionSchema = new Schema({
    userId: {
        required: true,
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    read: {
        default: false,
        type: Boolean,
    },
    message: {
        type: String,
        required: true
    },
    activity: {
        title: {
            type: String,
        },
        goal: {
            type: String,
        },
        material: {
            type: String,
        },
        tooHard: {
            type: String,
        },
        tooEasy: {
            type: String,
        },
        setUp: {
            type: String,
        },
        activity: {
            type: String,
        },
        duration: {
            type: Number,
        },
        amountOfPeople: {
            type: Number,
        },
        wheelchair: {
            type: Boolean,
        },
        pointsForAttention: {
            type: String,
        },
    },
})

const Suggestion = mongoose.model('suggestions', SuggestionSchema)

module.exports = Suggestion