const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs'); 

const UserSchema = new Schema({
    email: {
        unique: true,
        type: String,
        required: [true, 'email is required']
    },
    password: {
        type: String,
        required: [true, 'password is required']
    },
    passwordResetToken: String,
    passwordResetExpires: Date,
    isVerified: { type: Boolean, default: false },
    isAdmin: { type: Boolean, default: false },
})

UserSchema.pre('save', async function(next) {
    try {

        const salt = await bcrypt.genSalt(10);

        const passwordHash = await bcrypt.hash(this.password, salt);
        console.log(passwordHash)
        console.log(this.password)
        this.password = passwordHash;
        console.log(this.password)

        next();
    } catch (error) {
        next(error);
    }
})

UserSchema.methods.isValidPassword = async function(newPassword){
    try {
        return await bcrypt.compare(newPassword, this.password);
    } catch (error){
        throw new Error(error);
    }
}

const User = mongoose.model('users', UserSchema)



module.exports = User