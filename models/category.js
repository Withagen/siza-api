const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CategorySchema = new Schema({
    name: {
        type: String,
        required: true
    },
    image: {
        type: String,
        //required: true
    },
    parent: {
        type: Schema.Types.ObjectId,
        ref: 'categories'
    }
})

const Category = mongoose.model('categories', CategorySchema)

module.exports = Category