const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const FeedbackSchema = new Schema({
    userId: {
        required: true,
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    message: {
        type: String,
        required: true
    },
    parentId: {
        type: Schema.Types.ObjectId,
        ref: 'feedback'
    },
    activityId: {
        required: true,
        type: Schema.Types.ObjectId,
        ref: 'activities'
    },
})

const Feedback = mongoose.model('feedback', FeedbackSchema)

module.exports = Feedback