const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ActivitySchema = new Schema({
    title: {
        type: String,
        required: true
    },
    goal: {
        type: String,
    },
    image: {
        type: String,
    },
    material: {
        type: String,
    },
    tooHard: {
        type: String,
    },
    tooEasy: {
        type: String,
    },
    setUp: {
        type: String,
    },
    setUpImage: {
        type: String,
    },
    activity: {
        type: String,
        required: true
    },
    duration: {
        type: Number,
    },
    amountOfPeople: {
        type: Number,
    },
    wheelchair: {
        type: Boolean,
    },
    pointsForAttention: {
        type: String,
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: 'categories'
    }
})

const Activity = mongoose.model('activities', ActivitySchema)

module.exports = Activity