const Category = require('../models/category')
var url = require('url') ;

const convertToCategoryQuery = (req) => {

    const hostname = req.headers.host; 
    const pathname = url.parse(req.url).pathname; 
    const categoryProps = req.body
    const name = categoryProps.name
    const image = process.env.NODE_ENV !== 'test' ? req.file ? req.file.filename : undefined : true
    const parent = categoryProps.parent

    const imageUrl = process.env.NODE_ENV !== 'test' ?  'https://' + hostname  + '/images/category/' + image : categoryProps.image

    const query = {}

    if (name) {
        query.name = name
    }

    if (image) {
        query.image = imageUrl
    }

    if (parent) {
        query.parent = parent
    }

    return query;

}

async function getChildrenRecursive(categoryId, next) {
    const ids = []
    ids.push(categoryId)
    const children = await Category.find({parent: categoryId})
    if (children.length > 0){
        for (const child of children){
            const result = await getChildrenRecursive(child._id)
            result.forEach(id => {
                ids.push(id)
            })
        }
    }     
    return ids
}

module.exports = {
    get(req, res, next) {
        Category.find().then(
            (categories) => {
                res.status(200).send({ categories: categories });
            }
        )
        .catch(next)
    },

    getWithoutChildrenOfCategory(req, res, next) {
        const id = req.params.id
        Category.findById(id).then(
            (category) => {
                getChildrenRecursive(category._id).then(
                    (ids) => {
                        Category.find({ _id: { $nin: ids } }).then(
                            (filteredCategories) => {
                                res.status(200).send({ categories: filteredCategories });
                            }
                        ).catch(next)
                    }
                )
            }
        )
        .catch(next)
    },

    getCategoriesWithoutParent(req, res, next) {
        Category.find({parent: null}).then(
            (categories) => {
                res.status(200).send({ categories: categories });
            }
        )
        .catch(next)
    },

    getCategoriesParentName(req, res, next)
    {
        Category.find()
        .populate('parent', 'name')
        .then(
            (categories) => {
                res.status(200).send({ categories: categories });
            }
        )
        .catch(next)
    },

    getSpecificCategory(req, res, next)
    {
        const id = req.params.id;

        Category.findOne({_id: id}).then(
            (category) => {
                res.status(200).send({ category: category });
            }
        )
        .catch(next)
    },

    getSpecificCategoryParentName(req, res, next)
    {
        const id = req.params.id;

        Category.findOne({_id: id})
        .populate('parent', 'name')
        .then(
            (category) => {
                res.status(200).send({ category: category });
            }
        )
        .catch(next)
    },

    getSubOfParent(req, res, next) {
        const parentId = req.params.parentId

        Category.find({ parent: parentId }).then(
            (categories) => {
                res.status(200).send({ subcategories: categories });
            }
        )
        .catch(next)
    },

    post(req, res, next) {
        Category.create(convertToCategoryQuery(req)).then(
            (category) => {
                res.status(200).send({ category: category });
            }
        )
        .catch(next)

    },

    put(req, res, next) {
        console.log()
        const categoryId = req.params.categoryId

        Category.update({_id: categoryId}, convertToCategoryQuery(req)).then(
            (result) => {
                Category.findById({_id: categoryId}).then(
                    (category) => {
                        res.status(200).send({ category: category });
                    }
                )
            }
        )
        .catch(next)

    },

    delete(req, res, next) {
        const categoryId = req.params.categoryId

        Category.findByIdAndRemove({_id: categoryId}).then(
            (category) => {
                res.status(200).send({ category: category });
            }
        )
        .catch(next)

    },
}