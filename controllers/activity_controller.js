const Activity = require('../models/activity')
var url = require('url') ;

const searchFilterQuery = (categoryId, wheelchair, minDuration, maxDuration, minAmountOfPeople, maxAmountOfPeople ) => {
    const query = {};
    console.log(categoryId)
    
    if (categoryId){
        query.category = categoryId   
    }
    if (wheelchair){
        query.wheelchair = wheelchair   
    }
    if (minDuration && maxDuration){
        query.duration = {
            $gte: minDuration,
            $lte: maxDuration
        }   
    }
    if (minAmountOfPeople && maxAmountOfPeople){
        query.amountOfPeople = {
            $gte: minAmountOfPeople,
            $lte: maxAmountOfPeople
        }   
    }

    return query
}


const convertToQueryActivity = (req) => {
    const hostname = req.headers.host; 
    const activityProps = req.body

    const isTestMode = process.env.NODE_ENV !== 'test'

    var imageName
    var setupImageName

    if (isTestMode) {
        req.files.forEach(file => {
            console.log(file.filename)
            console.log(file.filename.includes("activity"))
            console.log(file.filename.includes("setup"))
    
            if(file.filename.includes("activity")){
                imageName = file.filename
            }
            if(file.filename.includes("setup")){
                setupImageName = file.filename
            }
        });
    }
    
    // const imageName = isTestMode ? req.files[0] ? req.files[0].filename : undefined : undefined
    // const setupImageName = isTestMode ? req.files[1] ? req.files[1].filename : undefined : undefined
   
    const urlImage = 'http://' + hostname  + '/images/activity/'
    
    const image = isTestMode ? urlImage + imageName : activityProps.image
    const setUpImage = isTestMode ? urlImage + setupImageName : activityProps.setUpImage

    const query = {}

    if (activityProps.title){
        query.title = activityProps.title
    }

    if (activityProps.goal){
        query.goal = activityProps.goal
    }

    if (activityProps.material){
        query.material = activityProps.material
    }

    if (activityProps.tooHard) {
        query.tooHard = activityProps.tooHard
    }

    if (activityProps.tooEasy) {
        query.tooEasy = activityProps.tooEasy
    }

    if (activityProps.setUp) {
        query.setUp = activityProps.setUp
    }

    if (isTestMode){
        if (setupImageName) {
            console.log(setUpImage)
            query.setUpImage = setUpImage
        }

        if (imageName){
            console.log(image)
            query.image = image
        }
    } else {
        console.log('___')
        console.log(image)
        console.log(setUpImage)
        console.log('___')
        query.image = image
        query.setUpImage = setUpImage
    }

    if (activityProps.activity) {
        query.activity = activityProps.activity
    }

    if (activityProps.duration) {
        query.duration = parseInt(activityProps.duration)
    }

    if (activityProps.amountOfPeople) {
        query.amountOfPeople = parseInt(activityProps.amountOfPeople)
    }

    if (activityProps.wheelchair) {
        query.wheelchair = (activityProps.wheelchair == 'true')
    }

    if (activityProps.pointsForAttention) {
        query.pointsForAttention = activityProps.pointsForAttention
    }

    if (activityProps.category) {
        query.category = activityProps.category
    }


    return query;
}

module.exports = {
    getAll(req, res, next) {
        const wheelchair = req.query.wheelchair
        const minDuration = req.query.minDuration
        const maxDuration = req.query.maxDuration
        const minAmountOfPeople = req.query.minAmountOfPeople
        const maxAmountOfPeople = req.query.maxAmountOfPeople

        Activity.find(searchFilterQuery(null, wheelchair, minDuration, maxDuration, minAmountOfPeople, maxAmountOfPeople)).then(
            (activities) => {
                res.status(200).send({ activities: activities });
            }
        )
        .catch(next)
    },

    getAllSpecific(req, res, next) {
        const _id = req.params.categoryId
        const wheelchair = req.query.wheelchair
        const minDuration = req.query.minDuration
        const maxDuration = req.query.maxDuration
        const minAmountOfPeople = req.query.minAmountOfPeople
        const maxAmountOfPeople = req.query.maxAmountOfPeople

        Activity.find(searchFilterQuery(_id, wheelchair, minDuration, maxDuration, minAmountOfPeople, maxAmountOfPeople)).then(
            (activities) => {
                res.status(200).send({ activities: activities });
            }
        )
        .catch(next)
    },

    getSpecific(req, res, next) {
        const _id = req.params.activityId
        Activity.find({ '_id': { _id }}).then(
            (activity) => {
                res.status(200).send({ activity });
            }
        )
        .catch(next)
    },

    getOfCategory(req, res, next) {
        const category = req.params.categoryId
        Activity.find({category: category}).then(
            (activities) => {
                res.status(200).send({ activities: activities });
            }
        )
        .catch(next)
    },

    post(req, res, next) {
        Activity.create(convertToQueryActivity(req)).then(
            (activity) => {
                res.status(200).send({ activity: activity });
            }
        )
        .catch(next)

    },

    put(req, res, next) {
        const activityId = req.params.activityId
        Activity.update({_id: activityId}, convertToQueryActivity(req) ).then(
            (result) => {
                Activity.findById({_id: activityId}).then(
                    (activity) => {
                        res.status(200).send({ activity: activity });
                    }
                )
            }
        )
        .catch(next)

    },

    delete(req, res, next) {
        const activityId = req.params.activityId

        Activity.findByIdAndRemove({_id: activityId}).then(
            (activity) => {
                res.status(200).send({ activity: activity });
            }
        )
        .catch(next)

    },
}