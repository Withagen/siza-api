const randtoken = require('rand-token')
const JWT = require('jsonwebtoken');
const mongoose = require('mongoose')
var nodemailer = require('nodemailer')

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'noreply.siza.baf@gmail.com',
      pass: 'siza-baf-avans'
    }
  });

const Code = require('../models/code')
const VerifyToken = require('../models/verifyToken')
const User = require('../models/user')

var refreshTokens = {} 

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

signToken = (id) => {
    return JWT.sign({
        iss: 'Siza-API',
        sub: id,
        iat: new Date().getTime(), 
        exp: new Date().setDate(new Date().getDate() + 1)
    }, 'jfekfgoewihrugfbruierughqierub' );
};

module.exports = {
    decodeUserId  (req)  {
        return JWT.decode(req.headers['authorization'], {complete: true}).payload.sub;
    },

    header(req, res, next) {
        console.log(req.headers['authorization'])
        next();
    },
    
    confirmationPost(req, res, next) {
        const token = req.params.token
        VerifyToken.findOne({_id: token}).then(
            (foundVerifyToken) => {
                const timeDif =Math.abs(new Date().getTime() - foundVerifyToken.createdAt.getTime())
                const dif = Math.ceil(timeDif / (1000 * 3600 * 24))
                if (dif > 1) {
                    return res.status(401).send({ msg: 'Token expired' });
                } else {
                    User.findById({_id: foundVerifyToken._userId}).then(
                        (foundUser) => {
                            if (foundUser === null) {
                                return res.status(401).send({ msg: 'We were unable to find a user for this token.' });
                            }
                            if (foundUser.isVerified){
                                return res.status(200).send({ msg: 'User already verified' });
                            }
    
                            User.findByIdAndUpdate({ _id: foundUser._id }, { isVerified : true }).then(
                                (result) => {
                                    return res.status(200).send({ msg: "The account has been verified. Please log in." });
                                }
                            )
                            .catch(next)
                        }
                    )
                    .catch(next)
                }
                
            }
        )
        .catch(next)
    },

    resendTokenPost(req, res, next) {

    },

    // createCode(req, res, next){
    //     const numberOfDays = parseInt(req.params.days)
    
    //     const generatedCode = randtoken.uid(256)

    //     Code.findOne({code: generatedCode}).then(
    //         (found) => {
    //             console.log(found)
    //             console.log(found === null)
    //             if (found === null) {
    //                 console.log('inside')
    //                 var date = new Date();
    //                 console.log(date)
    //                 date.setDate(new Date().getDate() + numberOfDays)
    //                 console.log(date)
    //                 Code.create({ code: generatedCode, endDate: date}).then(
    //                     (code) => {
    //                         return res.status(200).send({ code: code });
    //                     }
    //                 )
                    
    //                 .catch(next)
                    
    //             } else {
    //                 module.exports.createCode(req, res, next)
    //             }

                
    //         }
    //     )
    //     .catch(next)
        
    // },

    // loginWithCode(req, res, next){
    //     const code = req.body.code

    //     Code.findOne({code: code}).then(
    //         (result) => {
    //             if (result !== null) {
    //                 return res.status(200).send({ succes: true });
    //             } else {
    //                 return res.status(401).send({ succes: false });
    //             }
    //         }
    //     )
    //     .catch(next)
    // },

    login (req, res, next) {
        const email = req.body.email
        console.log(email)
        
        User.findOne({email: email}).then((foundUser) => {
            if (foundUser !== null){
                if (foundUser.isVerified) {
                    const token = signToken(foundUser.id);
                    const refreshToken = randtoken.uid(256) 
                    refreshTokens[refreshToken] = email
                    res.status(200).send({ accesToken: token , refreshToken: refreshToken, user: foundUser });
                } else {
                    res.status(401).send({ 'msg' : 'users email not verified'})
                }   
            } else {
                res.status(401).send({ 'msg' : 'user not found'})
            }
        })
        .catch(next)
    },

    signup( req, res, next) {
        console.log(req.body)
        const email = req.body.email
        console.log(email)
        
        User.findOne({email: email}).then((foundUser) => {
            if (foundUser !== null){
                return res.status(400).send({ msg : `User with Email: ${email} already exists` });
            } else {
                User.create(req.body).then(
                    (createdUser) => {
                        console.log(createdUser)
                        VerifyToken.create({ _userId: createdUser._id }).then(
                            (createdVerifyToken) => {  
                                console.log(createdVerifyToken) 
                                if (process.env.NODE_ENV !== 'test'){
                                    var mailOptions = {
                                        from: 'youremail@gmail.com',
                                        to: email,
                                        subject: 'Siza - Confirm your email',
                                        text: 'Hello,\n\n' + 'Please verify your account by clicking the link: \nhttp:\/\/' + req.headers.host + '\/confirmation\/' + createdVerifyToken._id + '.\n' ,
                                        html: `<tr>
                                        <td> <a href="http://siza-baf.nl/auth/verifyemail/${createdVerifyToken._id}" target="_blank">Verify email</a> </td>
                                      </tr>`,
                                    };
                                    transporter.sendMail(mailOptions, function(error, info){
                                        if (error) {
                                            res.status(500).send({ msg : `${error.message}` });
                                        } else {
                                            res.status(200).send({ msg : `succes: ${ true }` });
                                        }
                                    });
                                } else {
                                    res.status(200).send({ msg : `succes: true` });
                                }
                                
                            }
                        )
                        .catch(next)
                    }
                )
                .catch(next)
            }
        })
        .catch(next)
    },

    refreshToken (req, res, next) {
        var email = req.body.email
        var refreshToken = req.body.refreshToken

        if((refreshToken in refreshTokens) && (refreshTokens[refreshToken] == email)) {
            User.findOne({email: email}).then((result) => {
                if (result !== null) {
                    const token = signToken(result._id);
                    return res.status(200).send({ accesToken: token });
                } else {
                    res.status(401).send()
                }
            })
        }
        else {
          res.status(401).send()
        }
    },

    jwt(req, res, next) {
        res.status(200).send();
    },

    registerNewAdmin(req, res, next) {
        const email = req.body.email
        console.log(`req.body : ${req.body}`)
        console.log(`req.body.email : ${req.body.email}`)
        console.log(`req.body.password : ${req.body.password}`)

        if (!validateEmail(email)){
            return res.status(422).send({ 'msg' : 'Email is not valid'})
        }

        User.find({email: email}).then((foundUsers) => {
            console.log(foundUsers)
            if (foundUsers.length !== 0)
            {
                console.log(foundUsers.length)
                return res.status(400).send({ 'msg' : `Admin with Email: ${email} already exists` });
            } else {
                
                User.create(req.body).then(
                    (admin) => {
                        console.log(admin._id)
                        const token = signToken(admin._id);
                        const refreshToken = randtoken.uid(256) 
                        refreshTokens[refreshToken] = email
                        return res.status(200).send({ accesToken: token , refreshToken: refreshToken });
                    }
                )
                .catch(next)
            }       
        })
        .catch(next)  
    },

    isAdmin(req, res, next){
        const userId = module.exports.decodeUserId(req)

        User.findById({ _id : userId }).then(
            (foundUser) => {
               if(foundUser.isAdmin){
                   next();
               } else {
                    res.status(401).send('access denied')
               }
               
            }
        )
    },

}

