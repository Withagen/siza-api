const User = require('../models/user')
const AuthController = require('../controllers/auth_controller')

module.exports = {
    getAll(req, res, next) {
        User.find().then(
            (users) => {
                res.status(200).send({'users' : users})
            }
        )
        .catch(next)
    },

    getMe(req, res, next) {
        const userId = AuthController.decodeUserId(req)

        User.findById({_id: userId}).then(
            (foundUser) => {
                if (foundUser) {
                    return res.status(200).send({user: foundUser});
                }

                return res.status(404).send({msg: `Geen gebruiker gevonden met id: ${userId}`})
            }
        )
        .catch(next)
        
    },

    getByIdAdmin(req, res, next) {
        User.findById({_id: req.params.id}).then(
            (foundUser) => {
                if (foundUser) {
                    return res.status(200).send({user: foundUser});
                }

                return res.status(404).send({msg: `Geen gebruiker gevonden met id: ${req.params.id}`})
            }
        )
    },

    getById(req, res, next) {
        User.findById({_id: req.params.id}, {email:1}).then(
            (foundUser) => {
                if (foundUser) {
                    return res.status(200).send({user: foundUser});
                }

                return res.status(404).send({msg: `Geen gebruiker gevonden met id: ${req.params.id}`})
            }
        )
    },

    getAdmins(req, res, next) {
        User.find({ isAdmin : true }).then(
            (users) => {
                res.status(200).send({'admins' : users})
            }
        )
        .catch(next)
    },

    setAdmin(req, res, next) {
        const userId = req.params.id
        const isAdmin = req.params.isAdmin === 'true'
        User.find({ isAdmin : true }).then(
            (users) => {
                if (users.length <= 1 && !isAdmin){
                    return res.status(400).send({'msg' : 'there must be at least one admin'})
                } else {
                    User.findByIdAndUpdate({_id: userId}, { isAdmin : isAdmin }).then(
                        (users) => {
                            User.findById(userId).then(
                                (updatedUser) => {
                                    res.status(200).send({'user' : updatedUser})
                                }
                            )
                            .catch(next)
                        }
                    )
                    .catch(next)
                }
            }
        )
        .catch(next)
        
    },
}