const Feedback = require('../models/feedback')
const AuthController = require('./auth_controller')
const User = require('../models/user')

async function deleteFeedback (id, next)  {
    Feedback.findByIdAndRemove({_id: id}).then(
        (res) => {
            return res;
        }
    )
    .catch(next)
}

module.exports = {
    getFeedbackOfActivity(req, res, next) {
        Feedback.find({'activityId': req.params.id}).then(
            (feedback) => {
                res.status(200).send({'feedback' : feedback})
            }
        )
        .catch(next)
    },

    postFeedback(req, res, next) {
        const userId = AuthController.decodeUserId(req)
        const body = req.body;
        const message = body.message;
        const activityId = req.params.activityId;
        const parentId = req.params.parentId;

       Feedback.create({userId: userId, activityId: activityId, message: message ? message : null, parentId: parentId ? parentId : null }).then(
           (feedback) => {
                res.status(200).send({'feedback' : feedback})
           }
       )
       .catch(next)
    },

    updateFeedback(req, res, next) {
        const userId = AuthController.decodeUserId(req)
        const id = req.params.id;
        Feedback.findById({_id: id}).then(
            (foundFeedback) => {
                if (!foundFeedback){
                    return res.send({'msg' : `Geen feedback gevonden met id : ${id}`})
                }
                
                if(foundFeedback.userId.toString() !== userId) {
                    return res.status(401).send({'msg' : 'acces denied'})
                } else {
                    Feedback.findByIdAndUpdate({_id: id}, req.body).then(
                        (result) => {
                            return res.status(200).send({'feedback' : result})
                        }
                    )
                    .catch(next)
                }
                
            }
        )
    },

    deleteFeedback(req, res, next) {
        const userId = AuthController.decodeUserId(req)
        const id = req.params.id;
        Feedback.findById({_id: id}).then(
            (foundFeedback) => {
                if (!foundFeedback){
                    res.send({'msg' : `Geen feedback gevonden met id : ${id}`})
                }
                
                if(foundFeedback.userId.toString() !== userId.toString()) {
                    User.findById({ _id : userId }).then(
                        (foundUser) => {
                           if(foundUser.isAdmin){
                               console.log('isAdmin')
                               deleteFeedback(id, next).then(
                                   (result) => {
                                        return res.status(200).send({'feedback' : result})
                                   }
                               )
                               .catch(next)
                           } else {
                                return res.status(401).send({'msg' : 'acces denied'})
                           }
                           
                        }
                    )
                    .catch(next)
                } else {
                    deleteFeedback(id, next).then(
                        (result) => {
                             return res.status(200).send({'feedback' : result})
                        }
                    )
                    .catch(next)
                }
                
            }
        )
        .catch(next)
    },


    

    
}