const Suggestion = require('../models/suggestion')
const AuthController = require('./auth_controller')

async function deleteSuggestion (id, next)  {
    Suggestion.findByIdAndRemove({_id: id}).then(
        (res) => {
            return res;
        }
    )
    .catch(next)
}

async function setRead (id, read, next)  {
    Suggestion.findByIdAndUpdate({_id: id}, {read: read}).then(
        (res) => {
            return res;
        }
    )
    .catch(next)
}

module.exports = {
    getAll(req, res, next) {
        Suggestion.find().then(
            (suggestions) => {
                res.status(200).send({'suggestions' : suggestions})
            }
        )
        .catch(next)
    },

    getSuggestionById(req, res, next) {
        Suggestion.findById({_id: req.params.id}).then(
            (foundSuggestion) => {
                res.status(200).send({'suggestion' : foundSuggestion})
            }
        )
        .catch(next)
    },

    updateRead(req, res, next) {
        const userId = AuthController.decodeUserId(req)
        const id = req.params.id
        const read = req.params.read

        Suggestion.findByIdAndUpdate({_id: id}, {read: read}).then(
            (result) => {
                return res.status(200).send({suggestion : result})
            }
        )
        .catch(next)
    },

    createSuggestion(req, res, next) {
        const userId = AuthController.decodeUserId(req)
        const body = req.body;
        const message = body.message;
        const activity = body.activity;

        Suggestion.create({'message' : message ? message : null, 'userId' : userId, 'activity' : activity ? activity : null }).then(
            (suggestion) => {
                res.status(200).send({ suggestion: suggestion });
            }
        )
        .catch(next)
    },

    deleteSuggestion(req, res, next) {
        const userId = AuthController.decodeUserId(req)
        const id = req.params.id;
        Suggestion.findById({_id: id}).then(
            (foundSuggestion) => {
                if (!foundSuggestion){
                    res.send({'msg' : `Geen Suggestion gevonden met id : ${id}`})
                }
                
                if(foundSuggestion.userId.toString() !== userId.toString()) {
                    User.findById({ _id : userId }).then(
                        (foundUser) => {
                           if(foundUser.isAdmin){
                               console.log('isAdmin')
                               deleteSuggestion(id, next).then(
                                   (result) => {
                                        return res.status(200).send({'suggestion' : result})
                                   }
                               )
                               .catch(next)
                           } else {
                                return res.status(401).send({'msg' : 'acces denied'})
                           }
                           
                        }
                    )
                    .catch(next)
                } else {
                    deleteSuggestion(id, next).then(
                        (result) => {
                             return res.status(200).send({'suggestion' : result})
                        }
                    )
                    .catch(next)
                }
                
            }
        )
        .catch(next)
    }

    
}