const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const cors = require('cors');
const mongoose = require('mongoose');
const activity_routes = require('./routes/activity_routes')
const category_routes = require('./routes/category_routes')
const auth_routes = require('./routes/auth_routes')
const admin_routes = require('./routes/admin_routes')
const feedback_routes = require('./routes/feedback_routes')
const suggestion_routes = require('./routes/suggestion_routes')
const User = require('./models/user')

const AuthController = require('./controllers/auth_controller')
const passport = require('passport');
const passportConf = require('./password'); //DONT REMOVE

mongoose.Promise = global.Promise;

console.log(process.env.NODE_ENV)
console.log(process.env.NODE_ENV !== 'test')

if (process.env.NODE_ENV !== 'test') {
    mongoose.connect('mongodb://localhost/siza-prod').then(() => {
        User.find().then(
            (users) => {
                if (users.length === 0){
                    const admin = new User({
                        email: 'marco.otten@siza.nl',
                        password: 'ArjWoiG9$',
                        isVerified: true,
                        isAdmin: true
                    })
                    User.create(admin).catch(
                        (err) => {
                            console.error(err)
                        }
                    )
                }
            }
        )
   })
}

// if (process.env.NODE_ENV !== 'test') {
//     mongoose.connect('mongodb+srv://myappdev:duwcas-vitsAf-hesfo1@cluster0-rkczc.gcp.mongodb.net/production?retryWrites=true&w=majority')
// }


app.use(cors());
app.use(bodyParser.json());


app.use('/images/category', express.static('images/category'))

app.use('/images/activity', express.static('images/activity'))

app.use('/v1/auth', auth_routes)
app.use('/v1/categories', category_routes)
app.use('/v1/activities', activity_routes)
app.use('/v1/suggestions', suggestion_routes)
app.use('/v1/feedback', feedback_routes)
app.use('/v1/admin', AuthController.header, passport.authenticate('jwt', { session: false }), AuthController.isAdmin,  admin_routes)

app.use((err, req, res, next) => {
    res.status(422).send({ msg: err.message });
});

module.exports = app;
