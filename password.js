const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const JWTStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
//const { JWT_SECRET } = require('./configuration/config');
const User = require('./models/user')

//JSON WEB TOKENS STRATEGY
passport.use( new JWTStrategy({
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: 'jfekfgoewihrugfbruierughqierub'
}, async (payload, done) => {
    try {
        console.log(ExtractJwt.fromHeader('authorization'))
        //Find admin of token
        User.findById(payload.sub).then((admin) => {
            if (admin === null) {
                return done(null, false);
            }

            done(null, admin);
        });

    } catch(error){
        console.log(ExtractJwt.fromHeader('authorization').prototype)
        done(error, false);
    }
}))

//LOCAL STRATEGY
passport.use(new LocalStrategy({
    usernameField: 'email',
}, async (email, password, done) => {
    try {
        User.findOne({ 'email': email })
        .then((admin) => {
            if (admin === null) {
                return done(null, false);
            }

            admin.isValidPassword(password)
                .then((isMatch) => {
                    console.log(`isMatch: ${isMatch}`);
                    if (!isMatch){
                        return done(null, false);
                    }
                    console.log(admin)
                    done(null, admin);
                })
            
        });
    } catch (error) {
        done(error, false);
    }
    
}));